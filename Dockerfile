FROM openjdk:10-jre-slim
WORKDIR /work
RUN set -x \
  && useradd -s /sbin/nologin stream2es \
  && apt-get update \
  && apt-get autoremove \
  && apt-get install -y curl \
  && curl --connect-timeout 3 --location --continue-at - --silent --output /work/stream2es.jar https://download.elasticsearch.org/stream2es/stream2es
USER stream2es
ENTRYPOINT ["/usr/bin/java", "-jar", "/work/stream2es.jar"]
