Dockerfile for elastic/stream2es
====

[elastic/stream2es](https://github.com/elastic/stream2es) を実行するコンテナを作るための Dockerfile 。

## Usage

### 1 ビルド

```bash
docker build -t stream2es:latest .
```

### 2 実行

```bash
docker container run --rm -it stream2es:latest --version
2018-08-02T02:07:22.468+0000 INFO  stream2es 20161020121123fe262bd
```
